# Modifications #
This dnEditor version is modified by kao. Original version is available at https://github.com/ViRb3/dnEditor

* * *

dnEditor
========

A .NET assembly editor based on dnlib.

Want to help with the development? You are more than welcome to do so!
Feel free to upgrade, fix or touch this project by submitting a pull request and I will 
make sure to approve and merge it.

Please note that I am working on this project during my free time, and as just a teenager doing everything
here alone, I cannot do it all instantly or at will. If your favourite feature is not found in dnEditor - submit
a ticket. And wait. I will eventually get through it.

#####Cheers!

#To-do (sorted by current priority):
 - [x] ~~Finish support for "switch" opcode.~~
 - [x] ~~Switch back to virtual TreeNode.~~
 - [x] ~~Add TreeView context menu with helpful options.~~
 - [x] ~~Add variables support.~~
 - [x] ~~Add support for "try-catch".~~
 - [ ] Add full search functions.
 - [ ] Add member overview/preview.
 - [ ] Add member icons.
 - [ ] Add resources support.

##Credits and thanks:
 - WiCKY Hu (Simple Assembly Explorer) for his awesome piece of work. I "borrowed" many ideas and features
 from there, for which I hope I won't have done bad.
 - 0xd4d (dnlib) for the epic library that made this whole project possible. Yes, if that didn't exist,
 this would have only been your fancy dream.
 - yck1509 (dnSpy) for the first dnlib-based assembly decompiler. That definitely served me as a
 reference of dnlib itself and what I wasn't sure how to do.
 - Sebastien LEBRETON (Reflexil) for the currently only fully-working and fully-featured assembly
 editor. If you find something similar in dnEditor's design: it's either Reflexil that inspired me
 or Reflexil that made me use the (almost) same design.
 
####All licenses can be found in the LICENSES.md file in the project root directory.
 
##Greetings:
 - To you for contributing, bug reporting or using this tool.
=======
dnEditor
========

A .NET assembly editor based on dnlib.

Want to help with the development? You are more than welcome to do so!
Feel free to upgrade, fix or touch this project by submitting a pull request and I will 
make sure to approve and merge it.

Please note that I am working on this project during my free time, and as just a teenager doing everything
here alone, I cannot do it all instantly or at will. If your favourite feature is not found in dnEditor - submit
a ticket. And wait. I will eventually get through it.

#####Cheers!

#To-do (sorted by current priority):
 - [x] ~~Finish support for "switch" opcode.~~
 - [x] ~~Switch back to virtual TreeNode.~~
 - [x] ~~Add TreeView context menu with helpful options.~~
 - [x] ~~Add variables support.~~
 - [x] ~~Add support for "try-catch".~~
 - [x] ~~Implement ILSpy decompiler.~~
 - [ ] Add full search functions.
 - [ ] Add member overview/preview.
 - [ ] Add member icons.
 - [ ] Add resources support.

##Credits and thanks:
 - WiCKY Hu (Simple Assembly Explorer) for his amazing piece of work. I "borrowed" many ideas and features
 from there, for which I hope I won't have done any bad.
 - 0xd4d (dnlib) for the epic library that made this whole project possible. Yes, if that didn't exist,
 this would have only been your fancy dream.
 - yck1509 (dnSpy & dnlib fork) for the first dnlib-based assembly decompiler. That definitely served me as a
 reference of dnlib itself and what I wasn't sure how to do. I am also using "a fork of his fork of dnlib",
 though I use none of his implementations. Still worth mentioning.
 - Sebastien LEBRETON (Reflexil) for the currently only fully-working and fully-featured assembly
 editor. If you find something similar in dnEditor's design: it's either Reflexil that inspired me
 or Reflexil that made me use the (almost) same design.
 - The guys behind ICSHARPCODE (ILSpy) [Daniel Grunwald, David Srbecky, Ed Harvey, Siegfried Pammer
 Artur Zgodzinski, Eusebiu Marcu, Pent Ploompuu] for their unbelievable work. 
 If they didn't keep their everything open-source I wouldn't have been able to add the decompiler for some 15 minutes.
 
####All licenses can be found in the LICENSES.md file in the project root directory.
 
##Greetings:
 - To you for contributing, bug reporting or using this tool.
 - To all boards, teams and individuals that support this work.